# `S^3`: Scalable Spike-and-Slab

These scripts reproduce the results of the article Scalable Spike-and-Slab (under review).

- 'scaling_simulations' folder contains code to produce Figure 1.
- 'comparisons' folder contains code to produce Figures 2 and 3.
- 'dataset_simulations' folder contains code to produce Figures 4 and 5.
